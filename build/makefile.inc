# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

CXX          = g++ -std=c++11
CXXCPP       = g++ -std=c++11 -E
CPPFLAGS     = -DFINTEGER=int  -fopenmp 
CXXFLAGS     = -fPIC -m64 -Wno-sign-compare -g -O3 -Wall -Wextra
CPUFLAGS     = -mpopcnt -msse4
LDFLAGS      = -fopenmp  
LIBS         = -lcblas -lf77blas -latlas -llapack  
PYTHONCFLAGS =  -I/usr/include/python3.5m -I/usr/include/python3.5m -I/home/yangna/.local/lib/python3.5/site-packages/numpy/core/include
SWIGFLAGS    = -DSWIGWORDSIZE64

NVCC         = 
CUDA_ROOT    = 
CUDA_ARCH    = default
NVCCFLAGS    = -I $(CUDA_ROOT)/targets/x86_64-linux/include/ \
-Xcompiler -fPIC \
-Xcudafe --diag_suppress=unrecognized_attribute \
$(CUDA_ARCH) \
-lineinfo \
-ccbin $(CXX) -DFAISS_USE_FLOAT16

OS = $(shell uname -s)

SHAREDEXT   = so
SHAREDFLAGS = -shared

ifeq ($(OS),Darwin)
	SHAREDEXT   = dylib
	SHAREDFLAGS = -dynamiclib -undefined dynamic_lookup
        SWIGFLAGS   =
endif

MKDIR_P      = /bin/mkdir -p
PYTHON       = python
SWIG         = swig
AR          ?= ar

prefix      ?= /home/yangna/chenjun/HISI3559a/faissx86
exec_prefix ?= ${prefix}
libdir       = ${exec_prefix}/lib
includedir   = ${prefix}/include
